//
//

#include <vector>
#include <string>
#include <stdexcept>

#pragma once

namespace castor {

class codec_error: public std::runtime_error {
public:
	codec_error(const char* reason):
	runtime_error(reason)
	{}

	codec_error(const std::string& reason)
	: runtime_error(reason)
	{}
};

struct hexa {
	static size_t encoded_size(size_t input);
	static size_t decoded_size(size_t input);

	static size_t encode(char* dst, const char* src, size_t len);
	static size_t decode(char* dst, const char* src, size_t len);

	static std::string encode(const std::vector<char>& bytes);
	static std::vector<char> decode(const std::string& str);
};

struct base64web {
	static size_t encoded_size(size_t input);
	static size_t decoded_size(size_t input);

	static size_t encode(char* dst, const char* src, size_t len);
	static size_t decode(char* dst, const char* src, size_t len);

	static std::string encode(const std::vector<char>& bytes);
	static std::vector<char> decode(const std::string& str);
};

struct hmac_sha256 {
	static std::vector<char> digest(const std::vector<char>& key, const std::vector<char>& data);
	static std::vector<char> digest(const std::string& key, const char* data, size_t data_len);
	static std::vector<char> digest(const char* key, size_t key_len, const char* data, size_t data_len);
};

} // castor
