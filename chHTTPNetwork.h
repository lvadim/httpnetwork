//
//  chNetwork.h
//  NetworkTest
//
//  Created by Vadim Luchko on 08.11.14.
//  Copyright (c) 2014 Vadim Luchko. All rights reserved.
//

/* КАК ОНО ДОЛЖНО РАБОТАТЬ
 
 В методе chHTTPThreadRoutine(), который крутится в отдельном потоке
 каждый раз вызываются методы Send()  Read(), котрорые отсылают данные из буффера httpDefaultBuffer[]
 и принимают ответы в него же.
 
 Есть очередь запросов и ответов.
 
 ....
*/

#ifndef __NetworkTest__chNetwork__
#define __NetworkTest__chNetwork__

const int chHTTPDefaultBufferSize = 8 * 1024;
const int chHTTPSleepTime = 20;

const int chHTTPMaxRequests = 8;

enum ERequestType
{
	ERT_SendUserInfo,
	ERT_SendScore,
	ERT_GetUserInfo,
	ERT_GetScore,
	
	ERT_Ivalid
};

struct TRequest
{
	ERequestType requestType;
	int requestLevel;
	unsigned long long fbId;
	
	TRequest()
	{
		requestType = ERT_Ivalid;
		requestLevel = 0;
		fbId = 0;
	};
};

//--- HTTP
class chHTTPNetwork
{
private:
	
	TRequest requestsPool[chHTTPMaxRequests];
	int requestsCount;
	
	static void * chHTTPThreadRoutine(void * data);
	
	void InitSocketSys();
    void CloseSocketSys();
	
	void OpenSock();
	void CloseSock();
	
	void Send();
	void Read();
	
	void AddRequest(ERequestType reqType_, int reqLevel_ = 0, unsigned long long fbId_ = 0);
	
	void FormMessage();
	
public:
    
    chHTTPNetwork();
    ~chHTTPNetwork();
	
	void SendUserInfo();
	void SendScore();
	void GetUserInfo(unsigned long long fbId_);
	void GetScore(unsigned long long fbId_);
};

chHTTPNetwork * GetHTTPNetwork();

#endif /* defined(__NetworkTest__chNetwork__) */
