//
//  chPlayer.h
//  NetworkTest
//
//  Created by vadim luchko on 11/25/14.
//  Copyright (c) 2014 Vadim Luchko. All rights reserved.
//

#ifndef __NetworkTest__chPlayer__
#define __NetworkTest__chPlayer__

#include <string>

const int c_friendsMAX = 64;

class chPlayer
{
public:
	std::string name;
	unsigned long long fbId;
	unsigned long long friends[64];
	int friendsCount;
	
	int score;
	
	chPlayer();
	~chPlayer(){}
	
	int FormInfoJson(char * jsonBuffer);
	int FormScoreJson(char * jsonBuffer);
	
};

chPlayer * GetMyPlayer();

#endif /* defined(__NetworkTest__chPlayer__) */