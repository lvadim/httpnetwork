//
//  chPlayer.cpp
//  NetworkTest
//
//  Created by vadim luchko on 11/25/14.
//  Copyright (c) 2014 Vadim Luchko. All rights reserved.
//

#include "chPlayer.h"

chPlayer myPlayer;

chPlayer * GetMyPlayer()
{
	return &myPlayer;
}

chPlayer::chPlayer()
{
	name = "Diego";
	fbId = 12345;
	memset(friends, 0, sizeof(friends));
	friendsCount = 0;
	score = 0;
}

int chPlayer::FormInfoJson(char * jsonBuffer)
{
	int j = sprintf(jsonBuffer, "{  \"facebook\": %llu, \"name\": \"%s\", \"facebook_friends\": [", GetMyPlayer()->fbId, GetMyPlayer()->name.c_str());
	for(int i = 0; i < GetMyPlayer()->friendsCount; ++i)
	{
		if(i == 0)
			j += sprintf(&jsonBuffer[j],"%llu", GetMyPlayer()->friends[i]);
		else
			j += sprintf(&jsonBuffer[j],",%llu", GetMyPlayer()->friends[i]);
	}
	j += sprintf(&jsonBuffer[j],"] }");
	
	return j;
}

int chPlayer::FormScoreJson(char * jsonBuffer)
{
	int j = sprintf(jsonBuffer, "{  \"facebook\": %llu, \"score\": %i }", GetMyPlayer()->fbId, score);
	
	return j;
}