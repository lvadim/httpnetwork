//
//  chNetwork.cpp
//  NetworkTest
//
//  Created by Vadim Luchko on 08.11.14.
//  Copyright (c) 2014 Vadim Luchko. All rights reserved.
//

#include "chHTTPNetwork.h"

#include "string.h"
#include "stdio.h"
#include "assert.h"

#include <string>

#ifndef _WIN32
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <pthread.h>
	#include <fcntl.h>
	#include <netdb.h>
	#include <errno.h>
	#include <unistd.h>
#else
	#include <windows.h>
	#include <winsock2.h>
	#include <process.h>
#endif

#include "chPlayer.h"
#include "codecs.h"

const char c_http_host[] = "104.236.33.52";
const int c_http_port = 9999;

bool threadRun = true;

char httpDefaultBuffer[chHTTPDefaultBufferSize];
int httpDefaultBufferSize = 0;

#ifndef _WIN32
	pthread_t threadId;
	pthread_mutex_t threadMutex;

	void chHTTPMutexLock() {pthread_mutex_lock(&threadMutex);}
	void chHTTPMutexUnlock() {pthread_mutex_unlock(&threadMutex);}
	void chSleep(unsigned int milliseconds) {usleep(milliseconds * 1000);}
#else
	u32 threadId;
	HANDLE threadMutex;

	void chHTTPMutexLock() {WaitForSingleObject(threadMutex, INFINITE);}
	void chHTTPMutexUnlock() {ReleaseMutex(threadMutex);}
	void chSleep(u32 milliseconds) {Sleep(milliseconds);}
#endif

//--- NETWORK----------------------------------

#ifdef _WIN32
	WSAData wsa;
	SOCKET sock;
#else
	int sock;

#endif

struct sockaddr_in addr;

chHTTPNetwork * currentNetwork = NULL;

chHTTPNetwork::chHTTPNetwork()
{
	currentNetwork = this;
    InitSocketSys();
    
#ifndef _WIN32
    pthread_mutex_init(&threadMutex, NULL);
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_create(&threadId, &attr, (void *(*)(void*))chHTTPThreadRoutine, NULL);
    pthread_attr_destroy(&attr);
#else
    threadMutex = CreateMutex(NULL, FALSE, NULL);
    _beginthread((void(_cdecl*)(void*)) , 0, NULL);
#endif
	
	requestsCount = 0;
}

chHTTPNetwork::~chHTTPNetwork()
{
    
}

void chHTTPNetwork::InitSocketSys()
{
#ifdef _WIN32
    WSADATA wsaData;
    if(WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
    {
        printf("WSAStartup failed: %s\n", WSAGetLastError());
        return;
    }
	
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_family = AF_INET;
	
#else

	
#endif
}

void chHTTPNetwork::CloseSocketSys()
{
#ifdef _WIN32
    if(WSACleanup() != 0)
    {
        printf("WSACleanup failed: %s\n", WSAGetLastError());
        return;
    }
#endif
}

void chHTTPNetwork::FormMessage()
{
	if(!requestsCount)
		return;
	
	const char TIME_SECRET[] = "change-me";
	
	const TRequest & curRequest = requestsPool[requestsCount - 1];
	
	char tJson[4096];
	
	switch (curRequest.requestType)
	{
		case ERT_SendUserInfo:
		{
			int jsonSize = GetMyPlayer()->FormInfoJson(tJson);
			
			auto computed_digest = castor::hmac_sha256::digest(TIME_SECRET, strlen(TIME_SECRET), tJson, jsonSize);
			std::string tB64 = castor::hexa::encode(computed_digest);
			
			httpDefaultBufferSize = sprintf(httpDefaultBuffer,"POST /user/info HTTP/1.1\r\nContent-Type: application/json\r\nHost: %s\r\ncontent-length:%i\r\nXDigest: %s\r\n\r\n", c_http_host, jsonSize, tB64.c_str());
			httpDefaultBufferSize += sprintf(&httpDefaultBuffer[httpDefaultBufferSize],"%s\r\n\r\n",tJson);
			break;
		}
			
		case ERT_SendScore:
		{
			int jsonSize = GetMyPlayer()->FormScoreJson(tJson);
			
			auto computed_digest = castor::hmac_sha256::digest(TIME_SECRET, strlen(TIME_SECRET), tJson, jsonSize);
			std::string tB64 = castor::hexa::encode(computed_digest);
			
			httpDefaultBufferSize = sprintf(httpDefaultBuffer,"POST /user/score HTTP/1.1\r\nContent-Type: application/json\r\nHost: %s\r\ncontent-length:%i\r\nXDigest: %s\r\n\r\n", c_http_host, jsonSize, tB64.c_str());
			httpDefaultBufferSize += sprintf(&httpDefaultBuffer[httpDefaultBufferSize],"%s\r\n\r\n",tJson);
			
			break;
		}
			
		case ERT_GetUserInfo:
			httpDefaultBufferSize = sprintf(httpDefaultBuffer,"GET /user/%llu/info HTTP/1.1\r\nContent-Type: application/json\r\nHost: %s\r\n\r\n",GetMyPlayer()->fbId, c_http_host);
			break;
			
		case ERT_GetScore:
			httpDefaultBufferSize = sprintf(httpDefaultBuffer,"GET /user/%llu/score HTTP/1.1\r\nContent-Type: application/json\r\nHost: %s\r\n\r\n",GetMyPlayer()->fbId, c_http_host);
			break;
			
		case ERT_Ivalid:
			printf("Invalid Request\n");
			assert(0);
			break;
		 
		default:
			printf("Invalid Request\n");
			assert(0);
			break;
	}
	
	--requestsCount;
}

void chHTTPNetwork::Send()
{
	if(!requestsCount)
	{
		return;
	}
	else
	{
		FormMessage();
	}
	
#ifdef _WIN32
	
//	memset(tBuff, 0, sizeof(tBuff));
//	host = gethostbyname(shost);
//	CopyMemory(&addr.sin_addr, host->h_addr_list[0],host->h_length);
//	addr.sin_port = htons(8888);
//	connect(sock, (struct sockaddr *) &addr, sizeof (addr));
//	send(sock, qHeader, strlen(qHeader), 0);
//	
//	recv(sock,tBuff,sizeof(tBuff),0);

#else
	
	int tResult = connect(sock,(struct sockaddr *)&addr, sizeof(addr));
	if(tResult < 0)
	{
		printf( "Error connecting: %i\n", errno);
		close( sock );
	}
	else
	{
		int tresult = send( sock, httpDefaultBuffer, httpDefaultBufferSize, 0 );
		printf("Send: %s\n\n", httpDefaultBuffer);
	}
#endif
		
}

void chHTTPNetwork::OpenSock()
{
	struct hostent * host = NULL;
	
	addr.sin_family = AF_INET;// use the Internet Address Family (IPv4)
	host = gethostbyname(c_http_host);// get host data from the host address
	bcopy( host->h_addr, &(addr.sin_addr.s_addr), host->h_length);// copy the address data from the host struct over to the server address struct
	addr.sin_port = htons( c_http_port );// set the port to connect to
	
	sock = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP ) ;
	
	struct timeval tv;
	
	//---timeout
	tv.tv_sec = 10;
	tv.tv_usec = 0;  // Not init'ing this can cause strange errors
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
}

void chHTTPNetwork::CloseSock()
{
	close( sock );
}

void chHTTPNetwork::Read()
{
	bzero( httpDefaultBuffer, chHTTPDefaultBufferSize);
	
	int tPos = 0;
	int tSize = recv( sock, &httpDefaultBuffer[tPos], chHTTPDefaultBufferSize - tPos, 0 );
	while (tSize)
	{
		tPos += tSize;
		tSize = recv( sock, &httpDefaultBuffer[tPos], chHTTPDefaultBufferSize - tPos, 0 );
	}
	
	httpDefaultBufferSize = tSize;
	
	printf("Recive:  %s\n", httpDefaultBuffer);
}

void chHTTPNetwork::AddRequest(ERequestType reqType_, int reqLevel_, unsigned long long fbId_)
{
	if(requestsCount >= chHTTPMaxRequests)
		return;
	
	requestsPool[requestsCount].requestType = reqType_;
	requestsPool[requestsCount].requestLevel = reqLevel_;
	requestsPool[requestsCount].fbId = fbId_;
	
	++requestsCount;
}

void chHTTPNetwork::SendUserInfo()
{
	AddRequest(ERT_SendUserInfo);
}

void chHTTPNetwork::SendScore()
{
	AddRequest(ERT_SendScore);
}

void chHTTPNetwork::GetUserInfo(unsigned long long fbId_)
{
	AddRequest(ERT_GetUserInfo, 0, fbId_);
}

void chHTTPNetwork::GetScore(unsigned long long fbId_)
{
	AddRequest(ERT_GetScore, 0, fbId_);
}

void * chHTTPNetwork::chHTTPThreadRoutine(void * data)
{
    chSleep(chHTTPSleepTime);
    
    while(threadRun)
    {
		GetHTTPNetwork()->OpenSock();
		
		GetHTTPNetwork()->Send();
		chSleep(chHTTPSleepTime);
		GetHTTPNetwork()->Read();
		chSleep(chHTTPSleepTime);
		
		GetHTTPNetwork()->CloseSock();
	}
    
    return NULL;
}

chHTTPNetwork * GetHTTPNetwork()
{
	return currentNetwork;
}