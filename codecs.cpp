//
//

#include "codecs.h"

#include <assert.h>
#include <algorithm>
#include <memory>
#include <utility>

#include "codecs/sha2code.h"

int modp_b16_encode(char* dest, const char* str, int len);
int modp_b16_decode(char* dest, const char* str, int len);

int modp_b64_encode(char* dest, const char* str, int len);
int modp_b64_decode(char* dest, const char* src, int len);



namespace castor {

// ****************************************************************************
//
// assumptions - no whitespaces are allowed within encoded string
//
// ****************************************************************************

size_t hexa::encoded_size(size_t input) {
	return input * 2;
}

size_t hexa::decoded_size(size_t input) {
	// in case the input size in odd, we are going to skip the last char anyway
	return (input + 1) / 2;
}

size_t hexa::encode(char* dst, const char* src, size_t len) {
	return static_cast<size_t>(modp_b16_encode(dst, src, static_cast<int>(len)));
}

size_t hexa::decode(char* dst, const char* src, size_t len) {
	return static_cast<size_t>(modp_b16_decode(dst, src, static_cast<int>(len)));
}

std::string hexa::encode(const std::vector<char>& bytes) {
	size_t expected = encoded_size(bytes.size());
	std::string result(expected, '=');
	encode(std::addressof(result.front()), bytes.data(), bytes.size());
	return result;
}

std::vector<char> hexa::decode(const std::string& str) {
	size_t expected = decoded_size(str.size());
	std::vector<char> result(expected, 0);
	size_t actual = decode(result.data(), std::addressof(str.front()), str.size());
	result.resize(actual);
	return result;
}

size_t base64web::encoded_size(size_t input) {
	return 4 * ((input + 2) / 3); 
}

size_t base64web::decoded_size(size_t input) {
	// in case the input size in odd, we are going to skip the last char anyway
	return (input / 4) * 3 + 2;
}

size_t base64web::encode(char* dst, const char* src, size_t len) {
	return static_cast<size_t>(modp_b64_encode(dst, src, static_cast<int>(len)));
}

size_t base64web::decode(char* dst, const char* src, size_t len) {
	int res = modp_b64_decode(dst, src, static_cast<int>(len));
	if (res == -1) {
		throw std::invalid_argument("bad char");
	}
	return static_cast<size_t>(res);
}

std::string base64web::encode(const std::vector<char>& bytes) {
	size_t expected = encoded_size(bytes.size());
	std::string result(expected, '=');
	encode(&result.front(), bytes.data(), bytes.size());
	return result;
}

std::vector<char> base64web::decode(const std::string& str) {
	size_t expected = decoded_size(str.size());
	std::vector<char> result(expected, '\0');
	size_t actual = decode(result.data(), &str.front(), str.size());
	result.resize(actual);
	return result;
}

struct sha256: SHA256_CTX {
	static const size_t block_size = SHA256_BLOCK_LENGTH;
	static const size_t digest_size = SHA256_DIGEST_LENGTH;

	sha256() {
		clear();
	}

	void clear() {
		crypto_SHA256_Init(this);
	}

	void update(const char* data, size_t length) {
		crypto_SHA256_Update(this, reinterpret_cast<const uint8_t*>(data), length);
	}

	void digest(std::vector<char>& result) {
		result.resize(digest_size, 0);
		crypto_SHA256_Final(reinterpret_cast<uint8_t*>(result.data()), this);
	}

	void digest(char result[digest_size]) {
		crypto_SHA256_Final(reinterpret_cast<uint8_t*>(result), this);
	}
};

std::vector<char> hmac_sha256::digest(const std::vector<char>& key, const std::vector<char>& data) {
	return digest(key.data(), key.size(), data.data(), data.size());
}

std::vector<char> hmac_sha256::digest(const std::string& key, const char* data, size_t data_len) {
	return digest(key.data(), key.length(), data, data_len);
}

std::vector<char> hmac_sha256::digest(const char* key, size_t key_len, const char* data, size_t data_len) {
	// restrict key to block len size
	assert(key_len != 0 && key_len <= sha256::block_size);

	// zero padded key
	char pkey[sha256::block_size];
	memset(pkey, 0, sha256::block_size);
	memcpy(pkey, key, key_len);
	// make key_ipad & key_opad
	char key_ipad[sha256::block_size];
	char key_opad[sha256::block_size];

	for(int n = 0; n != sha256::block_size; n++) {
		key_ipad[n] = pkey[n] ^ 0x36;
		key_opad[n] = pkey[n] ^ 0x5c;
	}

	// sha (ipad + data)
	char inner_digest[sha256::digest_size];

	sha256 hasher;
	hasher.update(key_ipad, sha256::block_size);
	hasher.update(data, data_len);
	hasher.digest(inner_digest);

	// sha (opad + inner_digest)
	std::vector<char> hmac_res;

	hasher.clear();
	hasher.update(key_opad, sha256::block_size);
	hasher.update(inner_digest, sha256::digest_size);
	hasher.digest(hmac_res);

	return hmac_res;
}


} // castor

